<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="assets/css/materialize.css">
  <link rel="stylesheet" href="assets/css/styles.css?v=<?php echo time(); ?>">
  <link rel="stylesheet" href="assets/js/aos/dist/aos.css">
  <link rel="icon" type="image/png" href="http://www.hihoudini.com/betah/assets/images/favicon.png" />
  <title>The Hacking Jerse - HOUDINI</title>
</head>
<body class="interna">
<!-- MENU -->
<div id="nav-icon3">
  <span></span>
  <span></span>
  <span></span>
  <span></span>
</div>
<nav class="nav">
  <ul>
    <li><a href=".">Home</a></li>
    <li><a href="contact">Contact</a></li>
    <li><a href="thehackingjersey">The Hacking Jersey</a></li>
  </ul>
</nav>
<!-- END MENU -->
<div class="intro">
  <div class="intro-tbl">
    <div class="intro-tbl-td">
      <div class="center-align">
        <div class="inlineb">
          <h1 class="no-margin" data-aos="fade-down" data-aos-duration="800"><a href="." title="Houdini"><img src="assets/images/houdini.png" srcset="assets/images/houdini.png 1x, assets/images/houdini@2x.png 2x,
            assets/images/houdini@3x.png 3x" alt="Houdini" width="320" class="block responsive-img"></a></h1>
          <div class="slogan" data-aos="zoom-in" data-aos-duration="800" data-aos-delay="100">is about to break free in <div class="inlineb main-counter" id="main-counter">09:00:00</div></div>
        </div>
        <span class="getintouch" id="getintouch" data-aos="fade" data-aos-duration="800" data-aos-delay="500" data-aos-offset="0"><span>GET IN TOUCH</span></span>
      </div>
    </div>
  </div>
</div>
<div class="bg-dash-black content-hacking-jersey" style="display:block">
  <div class="content-inner-interna">
    <div class="content-info-hacking-jersey">
      <article data-readmore>
        <h2 class="center no-margin title-view">The Hacking Jersey</h2>
        <div class="container-form">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/CvIsTG_054g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
        <p style="padding-bottom: 20px">La selección peruana se jugaba su clasificación al mundial Rusia 2018 a 10,000 Kms de distancia, en Wellington, Nueva Zelanda, una ciudad donde solo viven 64 peruanos. 
¿Cómo llevar el aliento de 30 millones de compatriotas y hacer sentir a nuestra selección en casa? Hackeando al estadio openente.
</p>
        <div class="content-text-hacking-jersey">
          <div class="item-text-hj clearfix">
            <h3>Momentun</h3>
            <div class="text-hj">
              El partido más importante de la selección peruana, en los últimos 35 años, se jugaba a más de 10,000 Kms de distancia, en Nueva Zelanda, millones de peruanos estarían atentos al duelo pero solo algunos podrían asistir.
            </div>
          </div>
          <div class="item-text-hj clearfix">
            <h3>Challenge</h3>
            <div class="text-hj">
              ¿Cómo llenar el Westpac Stadium de Wellington de peruanos, solo llevando a 11 personas?
            </div>
          </div>
          <div class="item-text-hj clearfix">
            <h3>Creatividad</h3>
            <div class="text-hj">
            Lo que motiva a una selección es ver a su barra de hinchas acompañandola a donde va. Esta vez nos tocó ir más lejos de lo esperado y pocos peruanos pudieron asistir al partido en el Westpac Stadium de Wellington.<br>
            Teníamos que apoderarnos del estadio.<br>
            Investigando descubrimos que la camiseta de NZ también es blanca, por eso el apelativo The All Whites. El rojo también es parte de sus diseños y su mapa es similar a una franja. Uniendo estos elementos creamos The AllBlanquirroja una camiseta con todos los elementos que todo hincha neozelandes enorgullece pero que a lo lejos se convierte en la camiseta peruana.<br><br>
  
            Hicimos que esta camiseta sea especial, una camiseta diseñada para el repechaje, una camiseta única que hicimos conocida a través de publidad geolocalizada y que repartimos exactamente el día del partido. Una idea pensada para el momento correcto y que mantuvimos en secreto hasta después del partido para que los Neozelandeses no estén advertidos.<br><br>
  
            The All Blanquirroja es una idea que demuestra que con Barrio somos capaces de hacer cualquier cosa, incluso llenar una tribuna de peruanos sin peruanos. 
  
            </div>
          </div>
  
        </div>
      </article>
    </div>
  </div>
</div>

<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/materialize.min.js"></script>
<script src="assets/js/jquery.countdown.min.js"></script>
<script src="assets/js/jquery.validate/jquery.validate.min.js"></script>
<script src="assets/js/readmore.min.js"></script>
<script src="assets/js/aos/dist/aos.js"></script>
<script src="assets/js/script.js?v=<?php echo time(); ?>"></script>
<script>
(function($) {
  $('article').readmore({
    collapsedHeight: 520,
    heightMargin: 200,
    spped: 600,
    moreLink: '<a href="#" class="view-more">Ver más</a>',
    lessLink: '<a href="#" class="less-text">Close</a>',
    afterToggle: function() {
      $('.view-more').hide();
    }
  });
})(jQuery);
</script>
</body>
</html>