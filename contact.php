<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="assets/css/materialize.css">
  <link rel="stylesheet" href="assets/css/styles.css?v=<?php echo time(); ?>">
  <link rel="stylesheet" href="assets/js/aos/dist/aos.css">
  <link rel="icon" type="image/png" href="http://hihoudini.com/assets/images/favicon.png" />
  <title>Contact - HOUDINI</title>
</head>
<body class="interna">
<!-- MENU -->
<div id="nav-icon3">
  <span></span>
  <span></span>
  <span></span>
  <span></span>
</div>
<nav class="nav">
  <ul>
    <li><a href=".">Home</a></li>
    <li><a href="contact">Contact</a></li>
    <li><a href="thehackingjersey">The Hacking Jersey</a></li>
  </ul>
</nav>
<!-- END MENU -->
<div class="intro">
  <div class="intro-tbl">
    <div class="intro-tbl-td">
      <div class="center-align">
        <div class="inlineb">
          <h1 class="no-margin" data-aos="fade-down" data-aos-duration="800"><a href="." title="Houdini"><img src="assets/images/houdini.png" srcset="assets/images/houdini.png 1x, assets/images/houdini@2x.png 2x,
            assets/images/houdini@3x.png 3x" alt="Houdini" width="320" class="block responsive-img"></a></h1>
          <div class="slogan" data-aos="zoom-in" data-aos-duration="800" data-aos-delay="100">is about to break free in <div class="inlineb main-counter" id="main-counter">09:00:00</div></div>
        </div>
        <span class="getintouch" id="getintouch" data-aos="fade" data-aos-duration="800" data-aos-delay="500" data-aos-offset="0"><span>GET IN TOUCH</span></span>
      </div>
    </div>
  </div>
</div>
<div class="content-form bg-dash-black" id="content-form" style="display:block">
  <span class="close-form" id="close-form" style="display:none">Close</span>
  <div class="content-form-tbl">
    <div class="content-tbl-td content-tbl-td-3">
        <div class="center-align">
          <div class="inlineb">
            <div class="content-hou">
                <div class="content-hou-tbl">
                  <div class="content-hou-tbl-td left-align">
                    <img src="assets/images/hou.png" srcset="assets/images/hou.png 1x, assets/images/hou@2x.png 2x, assets/images/hou@3x.png 3x" alt="Houdini" width="115" class="inlineb responsive-img">
                    <span class="inlineb connected">The<br>connected<br>agency</span>
                  </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <div class="content-tbl-td content-tbl-td-3">
      <div class="center-align">
        <div class="inlineb">
          <div class="three-getintouch">
              <div class="text-get left-align">GET</div>
              <div class="text-in right-align">IN</div>
              <div class="text-touch right-aligh">TOUCH</div>
          </div>
          <div class="list-address right-align">
            <div class="item-address">
                Based in Lima, Peru<br>
                Serving Global
            </div>
            <div class="item-address">
                <span class="Antwerp-bold ">Become a Client</span><br>
                <a href="mailto:santiago.rubio@hihoudini.com" class="link-email">santiago.rubio@hihoudini.com</a><br>
                <a href="tel:+51014223846">+51014223846</a>
            </div>
            <div class="item-address">
                <span class="Antwerp-bold ">Media inquiries</span><br>
                <a href="mailto:ask@hihoudini.com" class="link-email">ask@hihoudini.com</a><br>
                <a href="tel:+51014223846">+51014223846</a>
            </div>
            <div class="item-address">
                <span class="Antwerp-bold ">Jobs</span><br>
                <a href="mailto:ask@hihoudini.com" class="link-email">ask@hihoudini.com</a><br>
                <a href="tel:+51014223846">+51014223846</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="content-tbl-td content-tbl-td-6">
      <form action="#" method="post" class="form-contact" id="form-contact">
        <fieldset class="no-border no-padding">
          <div class="row-input row-input-middle input-field">
              <input id="name" name="name" type="text" class="required">
              <label for="name">Name</label>
          </div>
          <div class="row-input row-input-middle input-field">
              <input id="email" type="text" name="email" class="required email" >
              <label for="email">Email</label>
          </div>
          <div class="row-input input-field">
              <textarea id="message" name="message" class="materialize-textarea required" maxlength="300"></textarea>
              <label for="message">Message</label>
          </div>
          <div class="row-input input-field">
              <button type="submit" id="btn-send" class="btn-send">SEND</button>
          </div>
        </fieldset>
      </form>
      <div id="form-message" style="display:none">
        Your message has been sent. We’ll write you soon
      </div>
    </div>
  </div>
</div>

<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/materialize.min.js"></script>
<script src="assets/js/jquery.countdown.min.js"></script>
<script src="assets/js/jquery.validate/jquery.validate.min.js"></script>
<script src="assets/js/aos/dist/aos.js"></script>
<script src="assets/js/script_post.js?v=<?php echo time(); ?>"></script>
</body>
</html>