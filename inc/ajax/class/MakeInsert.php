<?php

include "dbclass.php";
require('config.inc.php');

class MakeInsert{

//    public function __construct($directory,$log_directory) {

    public function __construct() {

        $this->db = new DBClass();
    }

    public function __destruct() {
    
        $this->db->disconnect();
    }

    public function execute($sql_get_keys) {
       $exec;
//        try {
            $exec = $this->db->exec($sql_get_keys);
//        }
//        catch(Exception $e) {
          if (trim($this->db->log_message)){
              $filename    = "ErrorInserted_".date("Ymd").".log";
              $this->MakeLogEntry($this->db->log_message,$filename);
          };
//        }

        return $exec;
    }

    public function insert_form_landing($data){
        $data['name'] = mysql_real_escape_string($data['name']);
        $data['email'] = mysql_real_escape_string($data['email']);
        $data['message'] = mysql_real_escape_string($data['message']);

        $sql = 'INSERT INTO smrt_landing ';
        $sql.= '(name, email,message, date_register, status) ';
        $sql.= "VALUES ('".$data['name']."', '".$data['email']."','".$data['message']."','".$data['date_register']."',1)";
        
        $response = $this->execute($sql);
        $id_form = mysql_insert_id($this->db->connection);
        return $id_form;
    }


    public function MakeLogEntry($string,$log_path) {
        $fp     = fopen($log_path,"a+");
        fwrite($fp,$string);
        fclose($fp);
    }
    
};

?>
