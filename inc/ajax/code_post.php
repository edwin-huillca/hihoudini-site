<?php

    ini_set('display_errors', '1');
    
    include("class/MakeInsert.php");
    require_once("class/user_agent.php");
    require_once ("swiftmailer/vendor/autoload.php");

    date_default_timezone_set('America/Lima');
    $date_register = date('Y-m-d h:i:s');
    $date_log = date('d/m/Y h:i:s');

    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];

    $data = array(
        'name' => $name,
        'email' => $email,
        'message' => $message,
        'date_register' => $date_register
    );

    $provider = new MakeInsert();
    $id_form = $provider->insert_form_landing($data);
    
    $full_data2 = $date_register.",".$name.",".$email.",".$message;
    if($archivo2 = fopen("../log/data-formulario.txt", "a+")) {
        fwrite($archivo2, $full_data2 ."\r\n");
        fclose($archivo2);
    }
    
    if(@$id_form > 0) {
        
        $objClass = new stdClass();
        $objClass->id = $id_form;
        $objClass->estado = 1;
        $objClass->email = "error";

        /*** SENDGRID ***/

        // $text = "Hi!\nHow are you?\n";
        $text = "";
         // $html = "<html>
         //       <head></head>
         //       <body>
         //           <p>Hi!<br>
         //               How are you?<br>
         //           </p>
         //       </body>
         //       </html>";
         $html = "<html><head></head><body>";
         $html .= "<h3>Contact Form</h3>";
         $html .= "<strong>Name: </strong>".$name."<br/>";
         $html .= "<strong>Email: </strong>".$email."<br/>";
         $html .= "<strong>Message: </strong>".$message."<br/>";
         $html .= "</body></html>";

         // This is your From email address
         $from = array($email => $name);
         // Email recipients
         $to = array(
            'edwin.huillca@hihoudini.com'=>'Houdini',
            'victor.huamani@hihoudini.com'=>'Houdini',
            'ask@hihoudini.com'=>'Houdini'
         );
         // Email subject
         $subject = 'Houdini - Get in touch';

         // Login credentials
         $username = 'azure_e88bad438a6fae9193cffd021bb1ce04@azure.com';
         $password = 'v5Ckfli=h4J*i&Sh';

         // Setup Swift mailer parameters
         $transport = Swift_SmtpTransport::newInstance('smtp.sendgrid.net', 587);
         $transport->setUsername($username);
         $transport->setPassword($password);
         $swift = Swift_Mailer::newInstance($transport);

         // Create a message (subject)
         $message = new Swift_Message($subject);

         // attach the body of the email
         $message->setFrom($from);
         $message->setBody($html, 'text/html');
         $message->setTo($to);
         $message->addPart($text, 'text/plain');

          // send message
         if ($recipients = $swift->send($message, $failures))
         {
             //echo 'Message sent out to '.$recipients.' users';
         } else {
             // echo "Something went wrong - ";
             // print_r($failures);
         }
        /*** END SENDGRID ***/

        $objClass->email = $email;

        echo json_encode($objClass);
        
    } else {
        $ua = new UserAgent();

        $canal = "";
        $medio = "";

        if($ua->is_mobile()){
            $canal ="MOVIL";
            $medio = $ua->platform();
        }else{
            $canal ="DESKTOP";
            $medio = $ua->browser();
        }

        //FILE  DE   AUDITORIA
        $full_data = $date_log.", BD, ".$canal.", ".$medio.", ".$name.", ".$email.", ".$message;
        if($archivo = fopen("../log/data-no-guardada.txt", "a+")) {
            fwrite($archivo, $full_data . "\r\n");
            fclose($archivo);
        }

        $objClass = new stdClass();
        $objClass->estado = 3;
        $objClass->mensajeError= "";

        echo json_encode($objClass);

    }
?>
