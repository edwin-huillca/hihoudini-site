(function($) {

  AOS.init();

  $('#main-counter').countdown('2018/06/25', function(event) {
    $(this).html(event.strftime('%I:%M:%S'));
  });

  $('#getintouch').on('click', function() {
    $('#content-form').fadeIn(300);
  });

  $('#close-form').on('click', function() {
    $('#content-form').fadeOut();
	});
	
	$('#nav-icon3').click(function(){
		$(this).toggleClass('open');
		$('body').toggleClass('open-menu')
	});

	//var url_base = "//www.hihoudini.com/";
	var url_base = "http://hihoudini.com/";
	$('#form-contact').validate({
		messages:{
	        name: {
	            
	        },
	        email: {
	            
	        },
	        message: {
	            
	        }
    	},
		submitHandler: function() {
	  	$.ajax({
	        url: url_base + "inc/ajax/code_post.php",
	        data: $('#form-contact').serialize(),
	        type: 'POST',
	        beforeSend: function( xhr ) {
	       		$('#btn-send').attr('disabled','disabled');
	        }
	    })
	    .done(function( data ) {
	    	var response = $.parseJSON(data);
	    	if (response.id > 0){
	    		$("#form-contact").hide();
	    		$("#form-message").show();
	    	}

	    	return false;
	    }); 
	}

	});
})(jQuery);